<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wp_obelinesdw4' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wZEz-Jjti(6Z^U`0$[T{uCyR$^-?V8g[e`vOf,g_.N1[LPOn[qZ:+xs$DNrv&.M,' );
define( 'SECURE_AUTH_KEY',  '[poY@3Yp=JA,QbC+^^NUrCm2Wdp&!&IploP}rx,jWc>%K&9M+A2~}2`[O2xouglk' );
define( 'LOGGED_IN_KEY',    ']F.I6MH^0NzO pl@wj[L5v@K9uM?foQ9HTd7_vGqxmSF}?W*k},vRW;SQ,F2D)yq' );
define( 'NONCE_KEY',        'Hjd4D+ O.7AF:>:qQ><3r!(X>TZ+x{C{4|p+vkLmB?7Zdr1TU]np1_&swsyaLq^-' );
define( 'AUTH_SALT',        '4inIKh}*,aS/I<]_i6F3]AnT;gs? !wdwj&D1K{wjLoZ?$m})4;F!>41}IAC$P*H' );
define( 'SECURE_AUTH_SALT', 'h#<iX `#0*g>KOA{L2VGk2K_D9QTnY >}RKI+!xyB`.Z0Nn>)o1SZUv=,*:Z#]bS' );
define( 'LOGGED_IN_SALT',   '`.aPJbGxrsgs&i|$p%Fe8Zh-r-Hmt^I-0LECSyH9%<RU$xu|w.[Y|>0E2A@.g?1D' );
define( 'NONCE_SALT',       'C)[JP5PM~DpBq:vCmo=+~<bT5&6FesYeL;cC(BULB &mkc_QQ5-Bct[[i)>t</kh' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'plop_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
