jQuery( function ($) {
	//ADD MEDIA UPLOADER/SELECTOR
	upload_image = ( text_input = null, id_input = null, preview = null ) => {
		const image = wp.media({
			title: 'Choisir une image', // mutiple: true if you want to upload multiple files at once
			multiple: false
		})
		.open()
		.on('select', () =>{
			// This will return the selected image from the Media Uploader, the result is an object
			// We convert uploaded_image to a JSON object to make accessing it easier
			const uploaded_image = image.state().get('selection').first().toJSON();
			if( text_input !== null )
			{
				$(text_input).val( uploaded_image.url );
			}
			if( preview !== null )
			{
				$(preview).attr( "src", uploaded_image.sizes.thumbnail.url );
			}
			if( id_input !== null )
			{
				$(id_input).val( uploaded_image.id );
			}
		});
	};

	delete_image = del => {
		del.parent().children('.image_path_text').val('');
		del.parent().slideToggle();
	}

	// protect user role ie fix :poop:
	$('select#role option[value="administrator"]').remove();
	$('select#role option[value=""]').remove();


	$('.model').css('display', 'none');

	$('.display_button').on( "click", function(e){
		e.preventDefault();
		const model = $(this).parent().find(".model").html();
		const p = '<p class="image_slider"></p>';
		const container = $(this).parent().append( p );
		const pnode = container.find(".image_slider").last().append( model );
		const upload = pnode.find('.media_upload_bko');
		const uploadBis = pnode.find('.media_galerie_bko');
		const uploadPreview = pnode.find('.media_preview_bko');
		const text_input = pnode.find('.image_path_text');
		const del = pnode.find('.delete_image_bko');
		upload.on('click', () => {
			upload_image( text_input );
		});
		uploadBis.on('click', () => {
			upload_image( null, text_input, uploadPreview );
		});
		del.on('click', () => {
			delete_image( del );
		});
	});

	$('.delete_image_bko').on( "click", function(){
		delete_image( $(this) );
	});

	$('.media_upload_bko').on( "click", function(){
		upload_image( $(this).parent().find('.image_path_text')  );
	});
	$('.media_galerie_bko').on( "click", function(){
		upload_image( null, $(this).parent().find('.image_path_text'), $(this).parent().find('.media_preview_bko') );
	});
	$('.media_upload_single_bko').on( "click", function(){
		upload_image( null, $(this).parent().find('.image_id') );
	});
	// SVG Preview in media galery
	if(document.getElementById('tmpl-attachmen'))
	{
		const el = document.getElementById('tmpl-attachment');
		const pos = el.outerHTML.indexOf('<# } else if ( \'image\' === data.type && data.sizes ) { #>');
		const text = '<# } else if ( \'svg+xml\' === data.subtype ) { #>\n' +
			'<div class="centered">\n' +
				'<img src="{{ data.url }}" class="thumbnail" draggable="false" />\n' +
			'</div>\n' +
			'<div class="filename">' +
				'<div>{{ data.filename }}</div>' +
			'</div>';
		el.outerHTML = [el.outerHTML.slice(0, pos), text, el.outerHTML.slice(pos)].join('');
	}
});
