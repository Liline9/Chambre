<?php
/*
Plugin Name:Mediaschool
Depends:
Provides: Notre Super Plugin!
Plugin URI: https://gitlab.com/bulko/open-source/bulkwplugin
Description: Notre Super Plugin!
Version: 1.0.0
Author: mediaschool
Author URI: https://www.bulko.net/
License: http://www.wtfpl.net/
*/
$bulkConfig = [
	'pluginSlug' => 'mediaschool',// name without space, uppercase or special char
	'pluginVersion' => '1.0.0',
	'wpMinimalVersion' => '5.0.0',
	'reCaptcha' => [
		'private' => 'xxxxxxxxxx',
		'public' => 'xxxxxxxxxx',
		'https' => false,
		'whiteList' => [ // Comment to disable white list
			'192.168.0.145',
			'127.0.0.1',
			'r-ro.local',
			'localhost',
			'::1',
		],
	],
	'adminFooter' => [
		'displayCopyright' => true,
		'copyrightName' => 'Mediaschool',
		'copyrightLink' => 'https://www.mediaschool.eu/',
	]
];
require_once(  plugin_dir_path( __FILE__ ) . '/class/BulkInit.php' );
$init = new BulkInit( $bulkConfig );
$pluginSlug = $bulkConfig['pluginSlug'];
/**
 * Create a plugin variable named like your plugin slug.
 * You can used it in your theme or in other plugin.
 *
 * ( ${slug}->{class}->{method}({param}); ).
 * ex: $bulkwplugin->exemple->retriveMeta( $post );
 */
$$pluginSlug = $init->build();
?>
