<?php
class Bedroom extends Core
{
	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://developer.wordpress.org/reference/functions/register_post_type/
	 * @return void
	 */
	public function createPostType(): Void
	{
		register_post_type( 'bedroom', [
			'labels' => [
				'name' => __( 'Bedrooms', 'bedroom' ),
				'singular_name' => __( 'Bedroom', 'bedroom' ),
				'add_new' => __( 'Add', 'bedroom' ),
				'add_new_item' => __( 'Ajouter une chambre', 'bedroom' ),
				'edit_item' => __( 'Actualiser une chambre', 'bedroom' ),
				'new_item' => __( 'Nouvelle chambre', 'bedroom' ),
				'view_item' => __( 'Voir chambre', 'bedroom' ),
				'search_items' => __( 'Chercher chambre', 'bedroom' ),
				'not_found' => __( 'Aucune chambre', 'bedroom' ),
				'not_found_in_trash' => __( 'Bedroom not found in trash', 'bedroom' ),
				'parent_item_colon' => __( 'Parent item', 'bedroom' ),
				'menu_name' => __( 'Bedrooms', 'bedroom' ),
			],
			'hierarchical' => false,
			'description' => __( 'BulkWPlugin bedroom customPostType', 'bedroom' ),
			'supports' => [
				'editor',
				'thumbnail',
				'title',
			],
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 20,
			'menu_icon' => 'dashicons-store',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post'
		] );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://developer.wordpress.org/reference/functions/add_meta_box/
	 * @return void
	 */
	public function addMetaBox(): Void
	{
		add_meta_box(
			'price-price',
			__( 'Price', 'bedroom' ),
			[ $this, 'priceHtml' ],
			'bedroom',
			'normal',
			'default'
		);

		add_meta_box(
			'nbbed-nbbed',
			__( 'Nbbed', 'bedroom' ),
			[ $this, 'nbbedHtml' ],
			'bedroom',
			'normal',
			'default'
		);

		add_meta_box(
			'surface-surface',
			__( 'Surface', 'bedroom' ),
			[ $this, 'surfaceHtml' ],
			'bedroom',
			'normal',
			'default'
		);

		add_meta_box(
			'etage-etage',
			__( 'Etage', 'bedroom' ),
			[ $this, 'etageHtml' ],
			'bedroom',
			'normal',
			'default'
		);

		add_meta_box(
			'nobedroom-nobedroom',
			__( 'Nobedroom', 'bedroom' ),
			[ $this, 'nobedroomHtml' ],
			'bedroom',
			'normal',
			'default'
		);

	}

	public function priceHtml(WP_Post $post): Void
	{
		wp_nonce_field( '_price_nonce', 'price_nonce' );
		?>
		<p>
			<label for="price">Prix: </label>
			<input type="text" name="mediaschool_price" placeholder="0.00" value="<?= $this->getMeta ('mediaschool_price'); ?>" >
		</p>
		<?php

	}

	public function nbbedHtml(WP_Post $post): Void
	{
		wp_nonce_field( '_nbbed_nonce', 'nbbed_nonce' );
		?>
		<p>
			<label for="nbbed">Nombre de lits: </label>
			<input type="text" name="mediaschool_nbbed" placeholder="0" value="<?= $this->getMeta ('mediaschool_nbbed'); ?>" >
		</p>
		<?php

	}

	public function surfaceHtml(WP_Post $post): Void
	{
		wp_nonce_field( '_surface_nonce', 'surface_nonce' );
		?>
		<p>
			<label for="surface">Surface: </label>
			<input type="text" name="mediaschool_surface" placeholder="0.00" value="<?= $this->getMeta ('mediaschool_surface'); ?>" >
		</p>
		<?php

	}

	public function etageHtml(WP_Post $post): Void
	{
		wp_nonce_field( '_etage_nonce', 'etage_nonce' );
		?>
		<p>
			<label for="etage">Étage: </label>
			<input type="text" name="mediaschool_etage" placeholder="0" value="<?= $this->getMeta ('mediaschool_etage'); ?>" >
		</p>
		<?php

	}

		public function nobedroomHtml(WP_Post $post): Void
	{
		wp_nonce_field( '_nobedroom_nonce', 'nobedroom_nonce' );
		?>
		<p>
			<label for="nobedroom">N°Chambre: </label>
			<input type="text" name="mediaschool_nobedroom" placeholder="0" value="<?= $this->getMeta ('mediaschool_nobedroom'); ?>" >
		</p>
		<?php

	}


	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @param int $post_id
	 * @see https://codex.wordpress.org/Function_Reference/update_post_meta
	 * @return boolean
	 */
	public function saveMetaData( Int $post_id ): Bool
	{
		if (
			( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			|| !current_user_can( 'edit_post', $post_id )
			|| !isset( $_POST['price_nonce'] )
			|| !isset( $_POST['nbbed_nonce'] )
			|| !isset( $_POST['surface_nonce'] )
			|| !isset( $_POST['etage_nonce'] )
			|| !isset( $_POST['nobedroom_nonce'] )
			|| !wp_verify_nonce( $_POST['price_nonce'], '_price_nonce')
			|| !wp_verify_nonce( $_POST['nbbed_nonce'], '_nbbed_nonce')
			|| !wp_verify_nonce( $_POST['surface_nonce'], '_surface_nonce')
			|| !wp_verify_nonce( $_POST['etage_nonce'], '_etage_nonce')
			|| !wp_verify_nonce( $_POST['nobedroom_nonce'], '_nobedroom_nonce')

		)
		{
			return false;
		}
		update_post_meta( $post_id, 'mediaschool_price', esc_attr( $_POST['mediaschool_price'] ) );

		update_post_meta( $post_id, 'mediaschool_nbbed', esc_attr( $_POST['mediaschool_nbbed'] ) );

		update_post_meta( $post_id, 'mediaschool_surface', esc_attr( $_POST['mediaschool_surface'] ) );

		update_post_meta( $post_id, 'mediaschool_etage', esc_attr( $_POST['mediaschool_etage'] ) );

		update_post_meta( $post_id, 'mediaschool_nobedroom', esc_attr( $_POST['mediaschool_nobedroom'] ) );
		return true;

	}



	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.0.0 Agc
	 * @see https://codex.wordpress.org/Function_Reference/register_taxonomy
	 * @return Void
	 */
	public function createPostTaxonomy(): Void
	{
		register_taxonomy(
			'taxonomy_bedroom',
			'bedroom',
			[
				'hierarchical' => true,
				'label' => __( 'Category bedroom', 'exemple'),
				'query_var' => true,
				'rewrite' => [
					'slug' => 'taxonomy_bedroom',
					'with_front' => false,
				]
			]
		);
		// Register hook to alowing meta overriding.
		$this->registerTaxonomyHook( 'taxonomy_bedroom' );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @see Core::getTaxonomyTerms()
	 * @param  Bool|boolean $hide_empty
	 * @return Array
	 */
	public function getTaxonomyExempleTerms( Bool $hide_empty = true ): Array
	{
		return (array) $this->getTaxonomyTerms( 'taxonomy_bedroom' );
	}
}
?>
