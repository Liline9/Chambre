<?php
class Product extends Core
{
	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://developer.wordpress.org/reference/functions/register_post_type/
	 * @return void
	 */
	public function createPostType(): Void
	{
		register_post_type( 'product', [
			'labels' => [
				'name' => __( 'Products', 'product' ),
				'singular_name' => __( 'Product', 'product' ),
				'add_new' => __( 'Add', 'product' ),
				'add_new_item' => __( 'Add product', 'product' ),
				'edit_item' => __( 'Update product', 'product' ),
				'new_item' => __( 'New product', 'product' ),
				'view_item' => __( 'Wiew product', 'product' ),
				'search_items' => __( 'Search product', 'product' ),
				'not_found' => __( 'Product not found', 'product' ),
				'not_found_in_trash' => __( 'Product not found in trash', 'product' ),
				'parent_item_colon' => __( 'Parent item', 'product' ),
				'menu_name' => __( 'Products', 'product' ),
			],
			'hierarchical' => false,
			'description' => __( 'BulkWPlugin product customPostType', 'product' ),
			'supports' => [
				'editor',
				'thumbnail',
				'title',
			],
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 20,
			'menu_icon' => 'dashicons-smiley',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post'
		] );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://developer.wordpress.org/reference/functions/add_meta_box/
	 * @return void
	 */
	public function addMetaBox(): Void
	{
		add_meta_box(
			'price-price',
			__( 'Price', 'product' ),
			[ $this, 'priceHtml' ],
			'product',
			'normal',
			'default'
		);

	}

	public function priceHtml(WP_Post $post): Void
	{
		wp_nonce_field( '_price_nonce', 'price_nonce' );
		?>
		<p>
			<label for="price">Prix: </label>
			<input type="text" name="mediaschool_price" placeholder="0.00" value="<?= $this->getMeta ('mediaschool_price'); ?>" >
		</p>
		<?php

	}



	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @param int $post_id
	 * @see https://codex.wordpress.org/Function_Reference/update_post_meta
	 * @return boolean
	 */
	public function saveMetaData( Int $post_id ): Bool
	{
		if (
			( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			|| !current_user_can( 'edit_post', $post_id )
			|| !isset( $_POST['price_nonce'] )
			|| !wp_verify_nonce( $_POST['price_nonce'], '_price_nonce')

		)
		{
			return false;
		}

		update_post_meta( $post_id, 'mediaschool_price', esc_attr( $_POST['mediaschool_price'] ) );
		return true;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.0.0 Agc
	 * @see https://codex.wordpress.org/Function_Reference/register_taxonomy
	 * @return Void
	 */
	public function createPostTaxonomy(): Void
	{
		register_taxonomy(
			'taxonomy_product',
			'product',
			[
				'hierarchical' => true,
				'label' => __( 'Category product', 'exemple'),
				'query_var' => true,
				'rewrite' => [
					'slug' => 'taxonomy_product',
					'with_front' => false,
				]
			]
		);
		// Register hook to alowing meta overriding.
		$this->registerTaxonomyHook( 'taxonomy_product' );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @see Core::getTaxonomyTerms()
	 * @param  Bool|boolean $hide_empty
	 * @return Array
	 */
	public function getTaxonomyExempleTerms( Bool $hide_empty = true ): Array
	{
		return (array) $this->getTaxonomyTerms( 'taxonomy_product' );
	}
}
?>
