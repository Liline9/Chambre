<?php
class BulkInit{
	private $BulkObj;
	private $bulkConfig;

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.1
	 * @return  void
	 */
	public function __construct( Array $bulkConfig )
	{
		// Make sure we don't expose any info if called directly
		if ( !function_exists( 'add_action' ) )
		{
			wp_die( 'Hi there!  I\'m just a plugin, not much I can do when called directly.' );
		}
		require_once( ABSPATH . '/wp-config.php' );
		$this->bulkConfig = $bulkConfig;
		define( 'BKO_PLUGIN_NAME', $bulkConfig['pluginSlug'] );
		define( BKO_PLUGIN_NAME . '_PLUGIN_URL', plugins_url(  '../../js/admin.js', __FILE__ ) );
		define( BKO_PLUGIN_NAME . '_VERSION', $bulkConfig['pluginVersion'] );
		define( BKO_PLUGIN_NAME . '_MINIMUM_WP_VERSION', $bulkConfig['wpMinimalVersion'] );
		define( BKO_PLUGIN_NAME . '_PLUGIN_CLASS_DIR', plugin_dir_path( __FILE__ ) );
		define( BKO_PLUGIN_NAME . '_DELETE_LIMIT', 100000 );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Tissot 0.1.0 ( 2019-02-14 )
	 *
	 * @return Object (Std)
	 */
	public function build(): Object
	{
		$this->initObj();
		$this->initHook();
		$this->initAdvancedSetter();
		return $this->BulkObj;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.1
	 * @return void
	 */
	public function initHook(): Void
	{
		foreach ( $this->BulkObj as $objKey => $obj )
		{
			if ( method_exists( $this->BulkObj->$objKey , 'hook') )
			{
				$this->BulkObj->$objKey->hook();
			}
		}
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.1
	 * @return Object collection of mixed obj BulkObj
	 */
	public function initObj(): Object
	{
		// First of all requier Core class
		require_once( constant( BKO_PLUGIN_NAME . '_PLUGIN_CLASS_DIR' ) . '/Core.php' );
		$BulkObj = [];
		$ClassList = array_diff(
			scandir( constant( BKO_PLUGIN_NAME . '_PLUGIN_CLASS_DIR' ) ),
			[ '..', '.', 'BulkInit.php', 'Core.php' ] // Ignore unwanted file
		);

		foreach ( $ClassList as $key => $ClassFile )
		{
			require_once( constant( BKO_PLUGIN_NAME . '_PLUGIN_CLASS_DIR' ) . $ClassFile );
			$ClassName = substr( $ClassFile, 0, -4 );
			$BulkObj[$ClassName] = new $ClassName();
		}
		$this->BulkObj = (object) $BulkObj;
		return $this->BulkObj;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Tissot 0.1.0 ( 2019-02-14 )
	 *
	 * @return Void
	 */
	public function initAdvancedSetter(): Void
	{
		if ( class_exists( 'ReCaptchaForm' ) && !empty( $this->bulkConfig['reCaptcha'] ) )
		{
			if(
				!empty( $this->bulkConfig['reCaptcha']['whiteList'] )
				&& in_array( $_SERVER['REMOTE_ADDR'], $this->bulkConfig['reCaptcha']['whiteList'] )
			)
			{
				$this->BulkObj->ReCaptchaForm->setTestMod();
			}
			else
			{
				$this->BulkObj->ReCaptchaForm->setPrivate( $this->bulkConfig['reCaptcha']['private'] );
				$this->BulkObj->ReCaptchaForm->setPublic( $this->bulkConfig['reCaptcha']['public'] );
				$this->BulkObj->ReCaptchaForm->setHttps( $this->bulkConfig['reCaptcha']['https'] );
			}
		}
		if (
			class_exists( 'User' )
			&& !empty( $this->bulkConfig['adminFooter'] )
			&& !empty( $this->bulkConfig['adminFooter']['displayCopyright'] )
			&& !empty( $this->bulkConfig['adminFooter']['copyrightName'] )
			&& !empty( $this->bulkConfig['adminFooter']['copyrightLink'] )
		)
		{
			$this->BulkObj->User->setCopyright( $this->bulkConfig['adminFooter']['displayCopyright'], $this->bulkConfig['adminFooter']['copyrightName'], $this->bulkConfig['adminFooter']['copyrightLink'] );
		}
		elseif( class_exists( 'User' ) )
		{
			$this->BulkObj->User->setCopyright( false );
		}
	}
}

/*
 * Helper
 */
/**
 * @author Golga <r-ro@bulko.net>
 * @since 0.1.0 ( 2019-05-24 )
 *
 * @param String $objName
 * @return Object
 */
function getBKObj( String $objName ): Object
{
	if ( class_exists( $objName ) )
	{
		return new $objName();
	}
	else
	{
		wp_die("Le sous plugin " . $objName . " est introuvable");
	}
}
?>
