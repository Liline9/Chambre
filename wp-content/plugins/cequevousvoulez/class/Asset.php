<?php
class Asset extends Core
{
	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.5
	 * @see https://github.com/Oyana/wp-colors
	 * @return void
	 */
	public function __construct()
	{
		return true;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.5
	 * @see https://github.com/Oyana/wp-colors
	 * @return void
	 */
	function loginLogo(): Void
	{
		echo '<style type="text/css">';
		echo '#login h1 a, .login h1 a {';
		echo 'background-image: url(' . get_stylesheet_directory_uri() . '/img/favicon.png);';
		echo 'padding-bottom: 0px;';
		echo '}';
		echo '</style>';
	}
	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.5
	 * @see https://github.com/Oyana/wp-colors
	 * @return void
	 */
	public function addCustomAdminThemes(): Void
	{
		//Model & Controller loading
		$suffix = is_rtl() ? '-rtl' : '';

		wp_admin_css_color(
			'backboard',
			__( 'BlackBoard' ),
			plugins_url( "../css/backboard$suffix.css", __FILE__ ),
			array( '#363b3f', '#018A00', '#01FF00' )
		);
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.5
	 * @see https://github.com/Oyana/wp-colors
	 * @return void
	 */
	public function admStyle(): Void
	{
		wp_enqueue_style( 'bulk-admin-style', plugins_url( "../css/admin.css", __FILE__ ) );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.1
	 * @return void
	 */
	public function admScript(): Void
	{
		wp_enqueue_media();
		wp_enqueue_script( 'bulk-admin-script', plugins_url(  '../js/admin.js', __FILE__ ), [], '20151215', true );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 0.5
	 * @see https://github.com/Oyana/wp-colors
	 * @return void
	 */
	public function hook(): Void
	{
		$this->hookingA( 'admin_enqueue_scripts', 'admScript' );
		$this->hookingA( 'admin_init', 'addCustomAdminThemes');
		$this->hookingA( 'login_enqueue_scripts', 'loginLogo' );
		$this->hookingA('admin_enqueue_scripts', 'admStyle');
		$this->hookingA('login_enqueue_scripts', 'admStyle');
	}
}
