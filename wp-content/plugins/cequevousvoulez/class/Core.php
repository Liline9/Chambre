<?php
/**
 * Use this class to declar global hook & helper.
 * They will be executed once by Core children.
 * If you want a single execution use Main.php.
 */
class Core{
	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1
	 * @return boolean
	 */
	public function __construct()
	{
		return true;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @param  String $type (action||filter)
	 * @param  String $location
	 * @param  Array||String $function
	 * @return Void
	 */
	public function globHooking( String $type = 'action', String $location = 'init', $function ): Void
	{
		$functionName = "add_" . $type;
		$functionName( $location, $function );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @param  String $location
	 * @param  Array||String $function
	 * @return Void
	 */
	public function globHookingA( String $location = 'init', $function ): Void
	{
		$this->globHooking( 'action', $location, [$this, $function] );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @param  String $location
	 * @param  Array||String $function
	 * @return Void
	 */
	public function globHookingF( String $location = 'init', $function ): Void
	{
		$this->globHooking( 'filter', $location, [$this, $function] );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @param  String $type (action||filter)
	 * @param  String $location
	 * @param  Array $function
	 * @return Void
	 */
	public function hooking( String $type = 'action', String $location = 'init', String $function ): Void
	{
		$this->globHooking( $type, $location, [$this, $function] );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @param  String $type (action||filter)
	 * @param  String $location
	 * @param  Array $function
	 * @return Void
	 */
	public function hookingA( String $location = 'init', String $function ): Void
	{
		$this->globHooking( 'action', $location, [$this, $function] );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @param  String $type (action||filter)
	 * @param  String $location
	 * @param  Array $function
	 * @return Void
	 */
	public function hookingF( String $location = 'init', String $function ): Void
	{
		$this->globHooking( 'filter', $location, [$this, $function] );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1
	 * @return Void
	 */
	public function hook(): Void
	{
		$this->hookingA( 'init', 'createPostType' );
		$this->hookingA( 'init', 'createPostTaxonomy' );
		$this->hookingA( 'add_meta_boxes', 'addMetaBox' );
		$this->hookingA( 'save_post', 'saveMetaData' );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.0.0 Agc
	 * @return Void
	 */
	public function createPostTaxonomy(): Void
	{
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.0.0 Peperiot
	 * @param String $taxonomy
	 * @return Void
	 */
	public function registerTaxonomyHook( String $taxonomy ): Void
	{
		$this->hookingA( $taxonomy . '_edit_form_fields', $taxonomy . 'MetaDataHtmlUpdate' );
		$this->hookingA( 'edited_' . $taxonomy, $taxonomy . 'SaveMetaData' );
		$this->hookingA( 'created_' . $taxonomy, $taxonomy . 'SaveMetaData' );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1
	 * @return boolean
	 */
	public function createPostType(): Void
	{
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.2 (11/07/2016 ce544a796a98a664a7109f58e49b0e7630a5a3fe)
	 * @see http://jeremyhixon.com/tool/wordpress-meta-box-generator/
	 * @param  String $value
	 * @return Mixed
	 */
	public function getMeta( $value )
	{
		/*
			Usage: getMeta( 'aa_meta_url' )
			Usage: getMeta( 'aa_meta_target' )
			Usage: getMeta( 'aa_meta_rel' )
			Usage: getMeta( 'aa_meta_class_css' )
			Usage: getMeta( 'aa_meta_title' )
		*/
		global $post;

		$field = get_post_meta( $post->ID, $value, true );
		if ( ! empty( $field ) )
		{
			return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
		}
		else
		{
			return false;
		}
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @return boolean
	 */
	public function addMetaBox(): Void
	{
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1
	 * @param  Int $post_id
	 * @return boolean
	 */
	public function saveMetaData( Int $post_id ): Bool
	{
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		{
			return false;
		}
		return true;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AGC 1.0.0
	 * @param  Array $args argument of wp_query
	 * @return Array of posts
	 */
	public function getWithMeta( Array $args ): Array
	{
		$posts = (array) get_posts( $args );
		return $this->retriveMeta( $posts );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AGC 1.0.0
	 * @param  Array $posts
	 * @return Array of posts
	 */
	public function retriveMeta( Array $posts ): Array
	{
		foreach ( $posts as $key => $post )
		{
			$posts[$key] = (array) $post;
			$metas = get_post_meta( $post->ID );
			foreach ( $metas as $k => $meta )
			{
				if ( isset( $meta[1] ) )
				{
					$posts[$key][$k] = $meta;
				}
				else
				{
					$posts[$key][$k] = $meta[0];
				}
			}
			if ( !empty( $posts[$key]["_thumbnail_id"] ) )
			{
				$posts[$key]["full-thumbnail"] = wp_get_attachment_image_src( $posts[$key]["_thumbnail_id"], 'single-post-thumbnail' )[0];
				$posts[$key]["thumbnail"] = wp_get_attachment_image_src( $posts[$key]["_thumbnail_id"], 'thumbnail' )[0];
				// $posts[$key]["agc-thumbnail"] = wp_get_attachment_image_src( $posts[$key]["_thumbnail_id"], 'agc-thumbnail' )[0];
			}
			$posts[$key]["permalink"] =  get_permalink( $post->ID );
			unset( $metas );
		}
		return $posts;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AGC 1.0.0
	 * @return Void
	 */
	public function customThumbnail(): Void
	{
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Peperiot 1.0.0
	 * @see https://developer.wordpress.org/reference/functions/get_terms/
	 * @param Bool $hide_empty
	 * @return Array
	 */
	public function getTaxonomyTerms( String $taxonomy, Bool $hide_empty = true ): Array
	{
		return (array) get_terms( [
			'orderby'		=> 'count',
			'taxonomy'		=> $taxonomy,
			'hide_empty'	=> $hide_empty,
		] );
	}
}
?>
