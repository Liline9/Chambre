# bulkWPlugin
> Wordpress sarter plugin.

## Provide:

* `mendatory` BulkInit.php *// Plugin initializer*.
* `mendatory` Core.php *// Custom coontent helper*.
* Main.php *// Script helper*.
* Asset.php *// BO color sheme generator & global assets enqueing*.
* MetaBox.php *// MetaBox content helper*.
* User.php *// basic user role config*.
* ReCaptchaForm.php *// admin login captcha*.
* Exemple.php *// Custom post type demo*.

## Dependencies:
> *if you want to used it with lower PHP or WordPress verssion you can check `legacy` branch.*

* WordPress >= 5.0.0
* PHP >= 7.2

## Installation:

1. Paste it in `wp-content/plugins`.
2. Rename the folder as your wish.
3. [optional] If tou have rename the folder you should rename `bulkwplugin.php` with the same name as your plugin folder.
4. [optional] Update configuration in `wp-content/plugins/{yourPluginName}/{yourPluginName}.php`.
5. [optional] Delete any class you dont want to use in your final project.
6. Start coding.

## How to used:

Any class in `class` folder will be automaticly loaded like a "sub-plugin".

You can used a lot of helper defined in `Core.php` by extending it.

You can easily create advanced [custom post type](https://codex.wordpress.org/Post_Types) by following the `exemple.php`.


## Licence


### DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE

TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

1. You just DO WHAT THE FUCK YOU WANT TO.

![Bulko logo](http://www.bulko.net/templates/img/bko.png "Bulko logo")
