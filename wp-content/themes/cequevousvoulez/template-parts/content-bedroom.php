<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
if ( !class_exists('Bedroom'))
{
	wp_die('la class bedroom est introuvable');
}
$bedroom = new Bedroom ();


?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
			the_content();
		?>
		<p>
			<?= $bedroom->getMeta('mediaschool_price'); ?> €
		</p>
		<p>
			<?= $bedroom->getMeta('mediaschool_nbbed'); ?> lit(s)
		</p>
		<p>
			<?= $bedroom->getMeta('mediaschool_surface'); ?> m²
		</p>
		<p>
			etage <?= $bedroom->getMeta('mediaschool_etage'); ?>
		</p>
		<p>
			n°<?= $bedroom->getMeta('mediaschool_nobedroom'); ?>
		</p>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
