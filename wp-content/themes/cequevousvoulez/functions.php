<?php
/**
 * functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */
define( 'THEME_NAME', 'cequevousvoulez' );
define( 'TPL_URI', get_template_directory_uri() );
define( 'TPL_PATH', get_template_directory() );
define( 'BASE_URL', get_home_url() );

if ( !function_exists( 'setup_theme_option' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function setup_theme_option() {
		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus([
			'menu-primary' => esc_html__( 'Primary', THEME_NAME ),
			'menu-footer' => esc_html__( 'Footer', THEME_NAME ),
			'menu-mobile' => esc_html__( 'Mobile', THEME_NAME )
		]);
	}
endif;
add_action( 'after_setup_theme', 'setup_theme_option' );

function disable_wp_emojicons()
{
	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
function disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) )
	{
		return array_diff( $plugins, array( 'wpemoji' ) );
	}
	else
	{
		return [];
	}
}

function disable_wp_bullshit()
{
	if ( !is_admin() )
	{
		wp_deregister_script('masonry');
		wp_deregister_script('jquery-masonry');
		wp_deregister_script('jquery');
		wp_deregister_script('wp-embed');
		disable_wp_emojicons();
	}
}

add_action( 'init', 'disable_wp_bullshit' );

/**
 *get_the_git_tkn
 *@author Golga <r-ro@bulko.net>
 *@since Peperiot 1.0.0
 *@return String
 */
function get_the_git_tkn()
{
	$headFetch = ".git/FETCH_HEAD";
	$headOrig = ".git/ORIG_HEAD";
	if ( file_exists($headFetch) && file_exists($headOrig) )
	{
		$fp = fopen( $headFetch, 'r' );
		$fp2 = fopen( $headOrig, 'r' );
		$headFetchData = fread($fp, 4096);
		$headOrigData = fread($fp2, 4096);
		return hash( 'ripemd160', "🦄____cequevousvoulez____🦄" . hash( 'haval160,4', $headFetchData ) . $headOrigData );
	}
	return "0.0.0";
}

/**
 *the_git_tkn
 *@author Golga <r-ro@bulko.net>
 *@since Peperiot 1.0.0
 *@return Void
 */
function the_git_tkn()
{
	echo get_the_git_tkn();
}

if ( !( is_admin() ) )
{
	function defer_parsing ( $url )
	{
		if ( FALSE === strpos( $url, '.js' ) )
		{
			return $url;
		}
		if ( strpos( $url, 'main.min.js' ) )
		{
			return "$url' defer data-turbolinks-eval='false' data-turbolinks-track='reload";
		}
		return "$url' data-turbolinks-eval='false";
	}
	add_filter( 'clean_url', 'defer_parsing', 11, 1 );
	/**
	 * Enqueue scripts and styles.
	 */
	function bulkWP_enque()
	{
		$tkn = get_the_git_tkn();
		wp_enqueue_style( 'cequevousvoulez', TPL_URI . '/css/main.css', $tkn );

		wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', [], $tkn, false );
		wp_enqueue_script( 'turbolinks', 'https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.2.0/turbolinks.js', ['jquery'], $tkn, false );
		wp_enqueue_script( 'ga', 'https://www.google-analytics.com/analytics.js', [], $tkn, false );
		wp_enqueue_script( 'cequevousvoulez', TPL_URI . '/jsMin/main.min.js', ['jquery', 'turbolinks'], $tkn, false );
		wp_deregister_script('contact-form-7');
		wp_deregister_script('maintenance-switch-button');
		wp_localize_script( 'cequevousvoulez', 'ajax_object', [
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'cequevousvoulez_root' => site_url(),
			'is_home' => is_front_page()
		]);
	}
	add_action( 'wp_enqueue_scripts', 'bulkWP_enque' );
}

function add_favicon()
{
	?>
	<link rel="apple-touch-icon" sizes="180x180" href="<?= TPL_URI ?>/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= TPL_URI ?>/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= TPL_URI ?>/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?= TPL_URI ?>/img/favicon/manifest.json">
	<link rel="mask-icon" href="<?= TPL_URI ?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
	<?php
}

add_action( 'wp_head', 'add_favicon' );
add_action( 'login_head', 'add_favicon' );
add_action( 'admin_head', 'add_favicon' );
